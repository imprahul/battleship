﻿using Battleship.Models;

namespace Battleship.Services.BoardServices
{
    public interface IBoardService
    {
        Board CreateBoard(int rows, int columns);
        Board Attack(Board board, Coordinates coordinates);
        bool IsAlive(Board board);
        Board PlaceShip(Board board, ShipState shipState);
    }
}