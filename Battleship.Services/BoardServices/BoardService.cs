﻿using System.Linq;
using Battleship.Factories.BoardFactories;
using Battleship.Models;
using Battleship.Models.Exceptions;
using Battleship.Services.ShipServices;

namespace Battleship.Services.BoardServices
{
    public class BoardService : IBoardService
    {
        private readonly IBoardFactory _boardFactory;
        private readonly IShipService _shipService;

        public BoardService(IBoardFactory boardFactory, IShipService shipService)
        {
            _boardFactory = boardFactory;
            _shipService = shipService;
        }
        public Board CreateBoard(int rows, int columns)
        {
            if (rows < 2 || columns < 2)
                throw new InvalidBoardSizeException();
            return _boardFactory.CreateBoard(rows, columns);
        }

        public Board Attack(Board board, Coordinates coordinates)
        {
            foreach (var shipState in board.ShipStates)
            {
                var attackedShipSquare =
                    shipState.ShipSquares.FirstOrDefault(
                        ss => ss.Coordinates.Row == coordinates.Row && ss.Coordinates.Column == coordinates.Column);
                if (attackedShipSquare != null)
                {
                    attackedShipSquare.ShipSquareState = ShipSquareState.Damaged;
                    break;
                }
            }
            return board;
        }

        public bool IsAlive(Board board)
        {
            return board.ShipStates.Any(t => _shipService.IsAlive(t));
        }

        public Board PlaceShip(Board board, ShipState shipState)
        {
            if(shipState.ShipSquares.Any(sq => sq.Coordinates.Row >= board.Rows || sq.Coordinates.Column >= board.Columns))
                throw new ShipOutOfBoundsException();

            if (_shipService.DoesShipOverlap(shipState, board.ShipStates))
                throw new ShipOverlapException();

            board.ShipStates.Add(shipState);
            return board;
        }
    }
}