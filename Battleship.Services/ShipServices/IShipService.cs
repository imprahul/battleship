﻿using System.Collections.Generic;
using Battleship.Models;

namespace Battleship.Services.ShipServices
{
    public interface IShipService
    {
        Ship CreateShip(int size);
        ShipState CreateShipState(Ship ship, Coordinates coordinates, ShipAlignment shipAlignment);
        bool IsAlive(ShipState shipState);
        bool DoesShipOverlap(ShipState newShipState, List<ShipState> existingShipStates);
    }
}