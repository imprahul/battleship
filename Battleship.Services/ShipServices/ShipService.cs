﻿using System.Collections.Generic;
using System.Linq;
using Battleship.Factories.ShipFactories;
using Battleship.Models;
using Battleship.Models.Exceptions;

namespace Battleship.Services.ShipServices
{
    public class ShipService : IShipService
    {
        private readonly IShipFactory _shipFactory;
        private readonly IShipSquaresFactory _shipSquaresFactory;

        public ShipService(IShipFactory shipFactory, IShipSquaresFactory shipSquaresFactory)
        {
            _shipFactory = shipFactory;
            _shipSquaresFactory = shipSquaresFactory;
        }

        public Ship CreateShip(int size)
        {
            if(size < 2)
                throw new InvalidShipSizeException();
            return _shipFactory.CreateShip(size);
        }

        public ShipState CreateShipState(Ship ship, Coordinates coordinates, ShipAlignment shipAlignment)
        {
            var shipState = new ShipState()
            {
                Ship = ship,
                ShipAlignment = shipAlignment,
                ShipSquares = _shipSquaresFactory.CreateShipSquares(coordinates, shipAlignment, ship.Size)
            };
            return shipState;
        }

        public bool IsAlive(ShipState shipState)
        {
            return shipState.ShipSquares.Any(square => square.ShipSquareState == ShipSquareState.Undamaged);
        }

        public bool DoesShipOverlap(ShipState newShipState, List<ShipState> existingShipStates)
        {
            return existingShipStates.Any(t => t.ShipSquares.Any(x => newShipState.ShipSquares.Any(
                n => n.Coordinates.Row == x.Coordinates.Row && n.Coordinates.Column == x.Coordinates.Column)));
        }
    }
}