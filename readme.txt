The solution consists of the following 5 projects

Battleship.Models
This project contains the domain models for the Battleship application

Battleship.Factories
This project contains factories that create the initial instance of the complex domain models.

Battleship.Service
This project contains services that serve the requests as per the requirements of the Battleship State Tracker exercise.

Battleship.Factories.Tests
These are unit test projects for the Factories.

Battleship.Services.Tests
These are unit test projects for the Services.


No UI or API has been implemented and no persistence layer is present.
