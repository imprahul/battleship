﻿using Battleship.Factories.ShipFactories;
using NUnit.Framework;

namespace Battleship.Factories.Tests
{
    [TestFixture]
    public class ShipFactoryTests
    {
        private IShipFactory _shipFactory;

        [SetUp]
        public void Setup()
        {
            _shipFactory = new ShipFactory();
        }

        [Test]
        [TestCase(5)]
        [TestCase(2)]
        [TestCase(7)]
        public void TestCreateShip(int size)
        {
            var ship = _shipFactory.CreateShip(size);

            Assert.AreEqual(ship.Size, size);
        }
    }
}