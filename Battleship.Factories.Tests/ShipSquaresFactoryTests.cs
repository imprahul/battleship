﻿using Battleship.Factories.ShipFactories;
using Battleship.Models;
using NUnit.Framework;

namespace Battleship.Factories.Tests
{
    [TestFixture]
    public class ShipSquaresFactoryTests
    {
        private IShipSquaresFactory _shipSquaresFactory;

        [SetUp]
        public void Setup()
        {
            _shipSquaresFactory = new ShipSquaresFactory();
        }

        [Test]
        [TestCase(1,1,3)]
        [TestCase(5, 5, 7)]
        public void TestCreateShipSquaresVertical(int row, int column, int shipSize)
        {
            var coordinates = new Coordinates() {Row = row, Column = column};

            var shipSquares = _shipSquaresFactory.CreateShipSquares(coordinates, ShipAlignment.Vertical, shipSize);

            Assert.AreEqual(shipSquares.Count, shipSize);

            for (int i = 0; i < shipSize; i++)
            {
                Assert.AreEqual(shipSquares[i].Coordinates.Row, row + i);
                Assert.AreEqual(shipSquares[i].Coordinates.Column, column);
            }
        }

        [Test]
        [TestCase(2, 5, 2)]
        [TestCase(1, 1, 3)]
        [TestCase(5, 5, 7)]
        public void TestCreateShipSquaresHorizontal(int row, int column, int shipSize)
        {
            var coordinates = new Coordinates() { Row = row, Column = column };

            var shipSquares = _shipSquaresFactory.CreateShipSquares(coordinates, ShipAlignment.Horizontal, shipSize);

            Assert.AreEqual(shipSquares.Count, shipSize);

            for (int i = 0; i < shipSize; i++)
            {
                Assert.AreEqual(shipSquares[i].Coordinates.Row, row);
                Assert.AreEqual(shipSquares[i].Coordinates.Column, column + i);
            }
        }
    }
}