﻿using Battleship.Factories.BoardFactories;
using Battleship.Factories.ShipFactories;
using NUnit.Framework;

namespace Battleship.Factories.Tests
{
    public class BoardFactoryTests
    {
        private IBoardFactory _boardFactory;

        [SetUp]
        public void Setup()
        {
            _boardFactory = new BoardFactory();
        }

        [Test]
        [TestCase(5,5)]
        [TestCase(2,19)]
        [TestCase(7,10)]
        public void TestCreateBoard(int rows, int columns)
        {
            var board = _boardFactory.CreateBoard(rows, columns);

            Assert.AreEqual(board.Rows,rows);
            Assert.AreEqual(board.Columns, columns);
        }
    }
}