﻿using Battleship.Models;

namespace Battleship.Factories.BoardFactories
{
    public interface IBoardFactory
    {
        Board CreateBoard(int rows, int columns);
    }
}