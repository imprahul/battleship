﻿using System.Collections.Generic;
using Battleship.Models;

namespace Battleship.Factories.BoardFactories
{
    public class BoardFactory : IBoardFactory
    {
        public Board CreateBoard(int rows, int columns)
        {
            return new Board()
            {
                Rows = rows,
                Columns = columns,
                ShipStates = new List<ShipState>()
            };
        }
    }
}