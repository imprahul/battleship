﻿using Battleship.Models;

namespace Battleship.Factories.ShipFactories
{
    public class ShipFactory : IShipFactory
    {
        public Ship CreateShip(int size)
        {
            return new Ship()
            {
                Size = size,
            };
        }
    }
}