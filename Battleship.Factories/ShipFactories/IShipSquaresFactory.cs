﻿using System.Collections.Generic;
using Battleship.Models;

namespace Battleship.Factories.ShipFactories
{
    public interface IShipSquaresFactory
    {
        List<ShipSquare> CreateShipSquares(Coordinates startCoordinates, ShipAlignment shipAlignment, int shipSize);
    }
}