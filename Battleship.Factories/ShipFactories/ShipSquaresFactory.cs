﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Battleship.Models;

namespace Battleship.Factories.ShipFactories
{
    public class ShipSquaresFactory : IShipSquaresFactory
    {
        public List<ShipSquare> CreateShipSquares(Coordinates startCoordinates, ShipAlignment shipAlignment, int shipSize)
        {
            var shipSquares = new List<ShipSquare>();
            for (var index = 0; index < shipSize; index++)
            {
                var columnOffset = shipAlignment == ShipAlignment.Horizontal ? index : 0;
                var rowOffset = shipAlignment == ShipAlignment.Vertical ? index : 0;
                shipSquares.Add(new ShipSquare()
                {
                    Coordinates = new Coordinates()
                    {
                        Column = startCoordinates.Column + columnOffset, Row = startCoordinates.Row + rowOffset
                    },
                    ShipSquareState = ShipSquareState.Undamaged
                });
            }
            return shipSquares;
        }
    }
}