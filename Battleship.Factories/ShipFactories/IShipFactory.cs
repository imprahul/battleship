﻿using Battleship.Models;

namespace Battleship.Factories.ShipFactories
{
    public interface IShipFactory
    {
        Ship CreateShip(int size);
    }
}