﻿using System;
using System.Collections.Generic;
using Battleship.Factories.ShipFactories;
using Battleship.Models;
using Battleship.Models.Exceptions;
using Battleship.Services.ShipServices;
using NSubstitute;
using NUnit.Framework;

namespace Battleship.Services.Tests
{
    [TestFixture]
    public class ShipServiceTests
    {
        private IShipFactory _shipFactory;
        private IShipSquaresFactory _shipSquaresFactory;
        private IShipService _shipService;
        [SetUp]
        public void Setup()
        {
            _shipFactory = Substitute.For<IShipFactory>();
            _shipSquaresFactory = Substitute.For<IShipSquaresFactory>();
            _shipService = new ShipService(_shipFactory, _shipSquaresFactory);
        }

        [Test]
        [TestCase(10)]
        [TestCase(20)]
        public void TestCreateShipValidSizeShipCreated(int size)
        {
            _shipFactory.CreateShip(size).Returns(new Ship() {Size = size});

            var ship = _shipService.CreateShip(size);
            Assert.AreEqual(size, ship.Size);
        }

        [Test]
        [TestCase(-10)]
        [TestCase(0)]
        [TestCase(1)]
        public void CanCreateShipInvalidSizeExceptionThrown(int size)
        {
            _shipFactory.CreateShip(size).Returns(new Ship() { Size = size });
            Assert.Throws<InvalidShipSizeException>(() => _shipService.CreateShip(size));
        }

        [Test]
        public void TestCreateShipStateVerticalShipStateCreated()
        {
            var ship = new Ship() {Size = 3};

            _shipSquaresFactory.CreateShipSquares(Arg.Any<Coordinates>(), Arg.Any<ShipAlignment>(), Arg.Any<int>())
                .Returns(new List<ShipSquare>()
                {
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 1}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 1}},
                });

            var shipState = _shipService.CreateShipState(ship, new Coordinates() {Row = 1, Column = 1},
                ShipAlignment.Vertical);

            Assert.AreEqual(shipState.ShipAlignment, ShipAlignment.Vertical);
            Assert.AreEqual(shipState.Ship.Size, ship.Size);
            Assert.AreEqual(shipState.ShipSquares.Count, 3);
            Assert.AreEqual(shipState.ShipSquares[0].Coordinates.Row, 1);
            Assert.AreEqual(shipState.ShipSquares[0].Coordinates.Column, 1);
            Assert.AreEqual(shipState.ShipSquares[1].Coordinates.Row, 2);
            Assert.AreEqual(shipState.ShipSquares[1].Coordinates.Column, 1);
            Assert.AreEqual(shipState.ShipSquares[2].Coordinates.Row, 3);
            Assert.AreEqual(shipState.ShipSquares[2].Coordinates.Column, 1);
        }

        [Test]
        public void TestCreateShipStateHorizontalShipStateCreated()
        {
            var ship = new Ship() { Size = 3 };

            _shipSquaresFactory.CreateShipSquares(Arg.Any<Coordinates>(), Arg.Any<ShipAlignment>(), Arg.Any<int>())
                .Returns(new List<ShipSquare>()
                {
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}},
                });

            var shipState = _shipService.CreateShipState(ship, new Coordinates() { Row = 1, Column = 1 },
                ShipAlignment.Vertical);

            Assert.AreEqual(shipState.ShipAlignment, ShipAlignment.Vertical);
            Assert.AreEqual(shipState.Ship.Size, ship.Size);
            Assert.AreEqual(shipState.ShipSquares.Count, 3);
            Assert.AreEqual(shipState.ShipSquares[0].Coordinates.Row, 1);
            Assert.AreEqual(shipState.ShipSquares[0].Coordinates.Column, 1);
            Assert.AreEqual(shipState.ShipSquares[1].Coordinates.Row, 1);
            Assert.AreEqual(shipState.ShipSquares[1].Coordinates.Column, 2);
            Assert.AreEqual(shipState.ShipSquares[2].Coordinates.Row, 1);
            Assert.AreEqual(shipState.ShipSquares[2].Coordinates.Column, 3);
            Assert.AreEqual(shipState.ShipSquares[0].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(shipState.ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(shipState.ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
        }

        [Test]
        public void TestIsAliveShipStateAllUndamagedReturnsTrue()
        {
            var shipState = new ShipState()
            {
                Ship = new Ship() {Size = 3},
                ShipAlignment = ShipAlignment.Horizontal,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}},
                }
            };

            var isAlive = _shipService.IsAlive(shipState);
            Assert.AreEqual(isAlive, true);
        }

        [Test]
        public void TestIsAliveShipStateSomeDamagedReturnsTrue()
        {
            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Horizontal,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}},
                }
            };

            var isAlive = _shipService.IsAlive(shipState);
            Assert.AreEqual(isAlive, true);
        }

        [Test]
        public void TestIsAliveShipStateAllDamagedReturnsFalse()
        {
            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Horizontal,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                    new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}, ShipSquareState = ShipSquareState.Damaged}
                }
            };

            var isAlive = _shipService.IsAlive(shipState);
            Assert.AreEqual(isAlive, false);
        }

        [Test]
        public void TestDoesShipOverlapReturnsTrueWhenShipsOverlap()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}, ShipSquareState = ShipSquareState.Damaged}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 3}, ShipSquareState = ShipSquareState.Damaged}
                    }
                }
            };

            var shipState = new ShipState()
            {
                Ship = new Ship() {Size = 3},
                ShipAlignment = ShipAlignment.Vertical,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 1, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 2, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 3, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    }
                }
            };

            var overlaps = _shipService.DoesShipOverlap(shipState, shipStates);

            Assert.AreEqual(overlaps, true);
        }

        [Test]
        public void TestDoesShipOverlapReturnsFalseWhenShipsDontOverlap()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}, ShipSquareState = ShipSquareState.Damaged}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 1}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 3}, ShipSquareState = ShipSquareState.Damaged}
                    }
                }
            };

            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Horizontal,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 5, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 5, Column = 2},
                        ShipSquareState = ShipSquareState.Damaged
                    }
                }
            };

            var overlaps = _shipService.DoesShipOverlap(shipState, shipStates);

            Assert.AreEqual(overlaps, false);
        }
    }
}
