﻿
using System.Collections.Generic;
using Battleship.Factories.BoardFactories;
using Battleship.Factories.ShipFactories;
using Battleship.Models;
using Battleship.Models.Exceptions;
using Battleship.Services.BoardServices;
using Battleship.Services.ShipServices;
using NSubstitute;
using NUnit.Framework;

namespace Battleship.Services.Tests
{
    [TestFixture]
    public class BoardServiceTests
    {
        private IBoardFactory _boardFactory;
        private IShipService _shipService;
        private IBoardService _boardService;

        [SetUp]
        public void Setup()
        {
            _shipService = Substitute.For<IShipService>();
            _boardFactory = Substitute.For<IBoardFactory>();
            _boardService = new BoardService(_boardFactory, _shipService);
        }

        [Test]
        [TestCase(10, 10)]
        [TestCase(10, 15)]
        [TestCase(2, 2)]
        [TestCase(3, 2)]
        [TestCase(2, 3)]
        public void TestCreateBoardValidBoardSize(int rows, int columns)
        {
            _boardFactory.CreateBoard(rows, columns).Returns(new Board() {Columns = columns, Rows = rows, ShipStates = new List<ShipState>()});

            var board = _boardService.CreateBoard(rows, columns);
            Assert.AreEqual(board.Rows, rows);
            Assert.AreEqual(board.Columns, columns);
            Assert.AreEqual(board.ShipStates.Count, 0);
        }

        [Test]
        [TestCase(1, 12)]
        [TestCase(2, 1)]
        [TestCase(0, 0)]
        [TestCase(-1, 5)]
        [TestCase(5, -1)]
        public void TestCreateBoardInvalidBoardSizeThrowsException(int rows, int columns)
        {
            _boardFactory.CreateBoard(rows, columns).Returns(new Board() { Columns = columns, Rows = rows });

            Assert.Throws<InvalidBoardSizeException>(() => _boardService.CreateBoard(rows, columns));
        }

        [Test]
        public void TestAttackUnoccupiedCoordinatesNoChangeToBoard()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var attackCoordinates = new Coordinates() {Row = 5, Column = 2};
            var newState = _boardService.Attack(board, attackCoordinates);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[0].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[0].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
        }

        [Test]
        public void TestAttackOccupiedCoordinatesSquaresDamaged()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var attackCoordinates = new Coordinates() { Row = 2, Column = 2 };
            var newState = _boardService.Attack(board, attackCoordinates);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[0].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[0].ShipSquareState, ShipSquareState.Damaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
        }

        [Test]
        public void TestAttackOccupiedCoordinatesDamagedNoChangeToBoard()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}, ShipSquareState = ShipSquareState.Damaged},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var attackCoordinates = new Coordinates() { Row = 5, Column = 2 };
            var newState = _boardService.Attack(board, attackCoordinates);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[0].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[0].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[0].ShipSquareState, ShipSquareState.Damaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[1].ShipSquareState, ShipSquareState.Undamaged);
            Assert.AreEqual(newState.ShipStates[1].ShipSquares[2].ShipSquareState, ShipSquareState.Undamaged);
        }

        [Test]
        public void TestIsAliveAllShipsAliveReturnsTrue()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            _shipService.IsAlive(Arg.Any<ShipState>()).Returns(true);

            var isAlive = _boardService.IsAlive(board);

            Assert.AreEqual(isAlive, true);
        }

        [Test]
        public void TestIsAliveOneShipsAliveReturnsTrue()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            _shipService.IsAlive(shipStates[0]).Returns(true);
            _shipService.IsAlive(shipStates[1]).Returns(false);

            var isAlive = _boardService.IsAlive(board);

            Assert.AreEqual(isAlive, true);
        }

        [Test]
        public void TestIsAliveAllShipsDeadReturnsFalse()
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            _shipService.IsAlive(Arg.Any<ShipState>()).Returns(false);

            var isAlive = _boardService.IsAlive(board);

            Assert.AreEqual(isAlive, false);
        }

        [Test]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        public void TestPlaceShipNoOverlapNoOutofBoundaryShipAdded(int startRowOfNewShip)
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Vertical,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip + 1, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip + 2, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    }
                }
            };

            var newBoard = _boardService.PlaceShip(board, shipState);

            Assert.AreEqual(newBoard.ShipStates.Count, 3);
            Assert.AreSame(newBoard.ShipStates[2], shipState);
        }

        [Test]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(10)]
        public void TestPlaceShipNoOverlapButOutofBoundaryThrowsException(int startRowOfNewShip)
        {
            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Vertical,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip + 1, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = startRowOfNewShip + 2, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    }
                }
            };

            Assert.Throws<ShipOutOfBoundsException>(() => _boardService.PlaceShip(board, shipState));
        }

        [Test]
        public void TestPlaceShipOverlapButNoOutofBoundaryThrowsException()
        {
            _shipService.DoesShipOverlap(Arg.Any<ShipState>(), Arg.Any<List<ShipState>>()).Returns(true);

            var shipStates = new List<ShipState>()
            {
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Horizontal,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 1}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 1, Column = 3}}
                    }
                }
                ,
                new ShipState()
                {
                    Ship = new Ship() { Size = 3 },
                    ShipAlignment = ShipAlignment.Vertical,
                    ShipSquares = new List<ShipSquare>()
                    {
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 2, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 3, Column = 2}},
                        new ShipSquare() {Coordinates = new Coordinates() {Row = 4, Column = 2}}
                    }
                }
            };

            var board = new Board()
            {
                Columns = 10,
                Rows = 10,
                ShipStates = shipStates
            };

            var shipState = new ShipState()
            {
                Ship = new Ship() { Size = 3 },
                ShipAlignment = ShipAlignment.Vertical,
                ShipSquares = new List<ShipSquare>()
                {
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 1, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 2, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    },
                    new ShipSquare()
                    {
                        Coordinates = new Coordinates() {Row = 3, Column = 1},
                        ShipSquareState = ShipSquareState.Damaged
                    }
                }
            };

            Assert.Throws<ShipOverlapException>(() => _boardService.PlaceShip(board, shipState));
        }
    }
}