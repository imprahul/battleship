﻿using System.Collections.Generic;

namespace Battleship.Models
{
    public class ShipState
    {
        public ShipAlignment ShipAlignment { get; set; }
        public Ship Ship { get; set; }
        public List<ShipSquare> ShipSquares { get; set; }
    }
}