﻿namespace Battleship.Models
{
    public enum ShipSquareState
    {
        Undamaged,
        Damaged
    }
}