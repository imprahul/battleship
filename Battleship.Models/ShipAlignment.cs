﻿namespace Battleship.Models
{
    public enum ShipAlignment
    {
        Vertical,
        Horizontal
    }
}