﻿using System;
using System.Collections.Generic;

namespace Battleship.Models
{
    public class Board
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public List<ShipState> ShipStates { get; set; }
    }
}
