﻿using System;

namespace Battleship.Models.Exceptions
{
    public class ShipOutOfBoundsException : Exception
    {
        public ShipOutOfBoundsException() : base("Ship is outside the boundary of the board.") { }
    }
}