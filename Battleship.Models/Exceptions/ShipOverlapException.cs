﻿using System;

namespace Battleship.Models.Exceptions
{
    public class ShipOverlapException : Exception
    {
        public ShipOverlapException () : base("This ship overlaps with other ships on the board!") { }
    }
}