﻿using System;

namespace Battleship.Models.Exceptions
{
    public class InvalidShipSizeException : Exception
    {
        public InvalidShipSizeException() : base("A ship must have a minimum size of 2 squares")
        {
            
        }
    }
}