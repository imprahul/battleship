﻿using System;

namespace Battleship.Models.Exceptions
{
    public class InvalidBoardSizeException : Exception
    {
        public InvalidBoardSizeException() : base("A board must have a minimum size of 2 rows and 2 columns")
        {

        }
    }
}