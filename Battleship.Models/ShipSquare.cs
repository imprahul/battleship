﻿namespace Battleship.Models
{
    public class ShipSquare
    {
        public Coordinates Coordinates { get; set; }
        public ShipSquareState ShipSquareState { get; set; }
    }
}