﻿using System.Security.AccessControl;

namespace Battleship.Models
{
    public class Coordinates
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public override bool Equals(object obj)
        {
            var coordinates = obj as Coordinates;
            if (coordinates == null)
                return false;
            return coordinates.Column == Column && coordinates.Row == Row;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}